package com.drsquad.android.provider.view.adapter.listener;


import com.drsquad.android.provider.model.dto.response.ScheduledListResponse;

public interface IScheduledListListener extends BaseRecyclerListener<ScheduledListResponse> {
}
