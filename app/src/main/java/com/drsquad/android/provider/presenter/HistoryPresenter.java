package com.drsquad.android.provider.presenter;

import android.os.Bundle;

import com.drsquad.android.provider.model.CustomException;
import com.drsquad.android.provider.model.HistoryModel;
import com.drsquad.android.provider.model.dto.common.HistoryItem;
import com.drsquad.android.provider.model.dto.response.HistoryResponse;
import com.drsquad.android.provider.model.listener.IModelListener;
import com.drsquad.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.drsquad.android.provider.view.adapter.HistoryAdapter;
import com.drsquad.android.provider.view.iview.IHistoryView;

import org.jetbrains.annotations.NotNull;

public class HistoryPresenter extends BasePresenter<IHistoryView> implements IHistoryPresenter {


    public HistoryPresenter(IHistoryView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    @Override
    public void onResume() {
        super.onResume();
        getHistory();
    }

    @Override
    public void getHistory() {
        new HistoryModel(new IModelListener<HistoryResponse>() {

            public void onClickItem(int pos, HistoryItem data) {
                iView.moveToChat(data);
            }

            @Override
            public void onSuccessfulApi(@NotNull HistoryResponse response) {
                iView.setAdapter(new HistoryAdapter(response.getHistory(), this::onClickItem));
                iView.dismissProgressbar();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHistoryDetails();
    }
}
