package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.ISplashPresenter;

public interface ISplashView extends IView<ISplashPresenter> {

    void startTimer(int splashTimer);

    void gotoLogin();

    void gotoHome();

}
