package com.drsquad.android.provider.presenter.ipresenter;

public interface ISplashPresenter extends IPresenter {

    boolean onCheckUserStatus();

    boolean hasInternet();

    void goToHome();

    void goToLogin();

}