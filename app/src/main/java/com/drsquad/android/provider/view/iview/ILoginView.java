package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
