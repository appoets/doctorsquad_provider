package com.drsquad.android.provider.presenter;

import com.drsquad.android.provider.presenter.ipresenter.IScheduleDialogPresenter;
import com.drsquad.android.provider.view.iview.IScheduleDialogView;


public class ScheduleDialogPresenter extends BasePresenter<IScheduleDialogView> implements IScheduleDialogPresenter {


    public ScheduleDialogPresenter(IScheduleDialogView iView) {
        super(iView);
    }

}
