package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.model.dto.response.InvoiceResponse;
import com.drsquad.android.provider.model.dto.response.ProfileResponse;
import com.drsquad.android.provider.presenter.ipresenter.IHomePresenter;

public interface IHomeView extends IView<IHomePresenter> {
        void updateUserDetails(ProfileResponse response);
        void goToHelp();
        void goToHistory();
        void goToSchedule();
        void goToAvailability();
        void setUp();
        void onViewDestroy();
        void showInvoice(InvoiceResponse response);
        void successRating(String message);
        void goToNotification();
        void updateNotification(int count);
}
