package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.model.dto.response.Provider;
import com.drsquad.android.provider.presenter.ipresenter.INotificationPresenter;
import com.drsquad.android.provider.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
    void showNotificationDialog(int pos, Provider data);
}
