package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
