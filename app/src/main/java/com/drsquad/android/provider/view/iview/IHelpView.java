package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.model.dto.response.HelpResponse;
import com.drsquad.android.provider.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
