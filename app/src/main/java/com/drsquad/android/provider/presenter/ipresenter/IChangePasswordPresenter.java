package com.drsquad.android.provider.presenter.ipresenter;

import com.drsquad.android.provider.model.dto.request.PasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void updatePassword(PasswordRequest request);
    void goToLogin();
}