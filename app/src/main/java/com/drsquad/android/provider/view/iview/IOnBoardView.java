package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.IOnBoardPresenter;

public interface IOnBoardView extends IView<IOnBoardPresenter> {

    void initSetUp();

    void gotoLogin();
}