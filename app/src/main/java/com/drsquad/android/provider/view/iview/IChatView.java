package com.drsquad.android.provider.view.iview;


import com.drsquad.android.provider.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
