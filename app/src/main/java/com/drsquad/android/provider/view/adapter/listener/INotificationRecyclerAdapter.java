package com.drsquad.android.provider.view.adapter.listener;

import com.drsquad.android.provider.model.dto.response.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<Provider> {
}
