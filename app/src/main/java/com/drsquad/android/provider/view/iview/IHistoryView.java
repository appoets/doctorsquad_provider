package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.model.dto.common.HistoryItem;
import com.drsquad.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.drsquad.android.provider.view.adapter.HistoryAdapter;

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryAdapter adapter);
    void initSetUp();
    void moveToChat(HistoryItem data);
}
