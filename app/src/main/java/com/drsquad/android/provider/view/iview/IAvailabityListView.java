package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.model.dto.response.AvailabilityListResponse;
import com.drsquad.android.provider.presenter.ipresenter.IAvailabilityListPresenter;

import java.util.List;

public interface IAvailabityListView extends IView<IAvailabilityListPresenter> {
    void setAdapter(List<AvailabilityListResponse> list);
    void moveToDetail(AvailabilityListResponse data);
    void initSetUp();
}
