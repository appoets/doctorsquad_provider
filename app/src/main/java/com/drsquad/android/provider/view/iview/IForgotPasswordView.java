package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
