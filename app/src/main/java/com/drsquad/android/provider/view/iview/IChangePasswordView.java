package com.drsquad.android.provider.view.iview;

import com.drsquad.android.provider.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
