package com.drsquad.android.provider.presenter;

import android.support.annotation.NonNull;

import com.drsquad.android.provider.model.CustomException;
import com.drsquad.android.provider.model.LoginModel;
import com.drsquad.android.provider.model.ProfileModel;
import com.drsquad.android.provider.model.dto.request.LoginRequest;
import com.drsquad.android.provider.model.dto.response.LoginResponse;
import com.drsquad.android.provider.model.dto.response.ProfileResponse;
import com.drsquad.android.provider.model.listener.IModelListener;
import com.drsquad.android.provider.presenter.ipresenter.ILoginPresenter;
import com.drsquad.android.provider.view.iview.ILoginView;

import org.jetbrains.annotations.NotNull;

import static com.drsquad.android.provider.MyApplication.getApplicationInstance;


public class LoginPresenter extends BasePresenter<ILoginView> implements ILoginPresenter {

    public LoginPresenter(ILoginView iView) {
        super(iView);
    }

    @Override
    public void goToForgotPassword() {
        iView.goToForgotPassword();
    }

    @Override
    public void goToRegistration() {
        iView.goToRegistration();
    }

    @Override
    public void postLogin(LoginRequest request) {
        iView.showProgressbar();
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                getApplicationInstance().setAccessToken(response.getAccessToken());
                getUserInfo();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).postLogin(request);
    }

    private void getUserInfo() {
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.dismissProgressbar();
                iView.goToHome();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getUserDetails();
    }
}
