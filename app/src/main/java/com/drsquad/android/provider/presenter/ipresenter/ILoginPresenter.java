package com.drsquad.android.provider.presenter.ipresenter;

import com.drsquad.android.provider.model.dto.request.LoginRequest;

public interface ILoginPresenter extends IPresenter {
    void postLogin(LoginRequest request);
    void goToRegistration();
    void goToForgotPassword();
}