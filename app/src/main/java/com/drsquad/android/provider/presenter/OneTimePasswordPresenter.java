package com.drsquad.android.provider.presenter;

import android.os.Bundle;

import com.drsquad.android.provider.presenter.ipresenter.IOneTimePasswordPresenter;
import com.drsquad.android.provider.view.iview.IOneTimePasswordView;


public class OneTimePasswordPresenter extends BasePresenter<IOneTimePasswordView> implements IOneTimePasswordPresenter {

    public OneTimePasswordPresenter(IOneTimePasswordView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }

    @Override
    public void goToForgotChangePassword() {
        iView.goToForgotChangePassword();
    }
}
