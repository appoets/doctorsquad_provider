package com.drsquad.android.provider.model;

import com.drsquad.android.provider.model.dto.response.HistoryResponse;
import com.drsquad.android.provider.model.listener.IModelListener;
import com.drsquad.android.provider.model.webservice.ApiClient;
import com.drsquad.android.provider.model.webservice.ApiInterface;

public class HistoryModel extends BaseModel<HistoryResponse>{

    public HistoryModel(IModelListener<HistoryResponse> listener) {
        super(listener);
    }

    public void getHistoryDetails(){
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getHistoryDetails());
    }

    @Override
    public void onSuccessfulApi(HistoryResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}

