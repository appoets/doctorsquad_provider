package com.drsquad.android.provider.view.adapter.listener;

import com.drsquad.android.provider.model.dto.common.HistoryItem;

public interface IHistoryRecyclerListener extends BaseRecyclerListener<HistoryItem> {
}
