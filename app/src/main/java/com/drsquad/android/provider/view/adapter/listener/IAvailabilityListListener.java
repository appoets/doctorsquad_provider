package com.drsquad.android.provider.view.adapter.listener;


import com.drsquad.android.provider.model.dto.response.AvailabilityListResponse;

public interface IAvailabilityListListener extends BaseRecyclerListener<AvailabilityListResponse> {
}
