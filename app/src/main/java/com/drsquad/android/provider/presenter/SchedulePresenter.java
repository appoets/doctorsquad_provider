package com.drsquad.android.provider.presenter;

import com.drsquad.android.provider.model.CustomException;
import com.drsquad.android.provider.model.ScheduleModel;
import com.drsquad.android.provider.model.dto.request.ScheduleRequest;
import com.drsquad.android.provider.model.dto.response.ScheduleResponse;
import com.drsquad.android.provider.model.listener.IModelListener;
import com.drsquad.android.provider.presenter.ipresenter.ISchedulePresenter;
import com.drsquad.android.provider.view.iview.IScheduleView;

import org.jetbrains.annotations.NotNull;

public class SchedulePresenter implements ISchedulePresenter {

    private IScheduleView iScheduleView;

    public SchedulePresenter(IScheduleView iScheduleView) {
        this.iScheduleView = iScheduleView;
    }

    @Override
    public void updateSchedule(ScheduleRequest request) {
        iScheduleView.showProgressbar();
        new ScheduleModel(new IModelListener<ScheduleResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull ScheduleResponse response) {
                iScheduleView.dismissProgressbar();
                iScheduleView.goToNotificationScreen(response.getMessage());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iScheduleView.dismissProgressbar();
                iScheduleView.showToast(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iScheduleView.dismissProgressbar();
                iScheduleView.showToast(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iScheduleView.dismissProgressbar();
            }
        }).updateSchedule(request);
    }

    @Override
    public void showCalendarDialog() {
        iScheduleView.showCalendarDialog();
    }

    @Override
    public void showTimePickerDialog() {
        iScheduleView.showTimeDialog();
    }
}
