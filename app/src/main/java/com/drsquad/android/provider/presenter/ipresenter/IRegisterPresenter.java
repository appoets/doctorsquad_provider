package com.drsquad.android.provider.presenter.ipresenter;

import com.drsquad.android.provider.model.dto.request.LoginRequest;
import com.drsquad.android.provider.model.dto.request.RegisterRequest;

public interface IRegisterPresenter extends IPresenter {
    void goToLogin();
    void getServiceData();
    void postLogin(LoginRequest request);
    void postRegister(RegisterRequest registerRequest);
}