package com.drsquad.android.provider.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.drsquad.android.provider.R;
import com.drsquad.android.provider.model.dto.response.ScheduledListResponse;
import com.drsquad.android.provider.view.adapter.listener.IScheduledListListener;
import com.drsquad.android.provider.view.adapter.viewholder.ScheduledListViewHolder;

import java.util.List;

public class ScheduledListAdapter extends BaseRecyclerAdapter<IScheduledListListener, ScheduledListResponse, ScheduledListViewHolder> {

    private List<ScheduledListResponse> scheduleList;
    private IScheduledListListener iScheduleListener;

    public ScheduledListAdapter(List<ScheduledListResponse> scheduleList, IScheduledListListener iScheduleListener) {
        super(scheduleList, iScheduleListener);
        this.scheduleList = scheduleList;
        this.iScheduleListener = iScheduleListener;

    }

    @NonNull
    @Override
    public ScheduledListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new ScheduledListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.schedule_list_item, parent, false), iScheduleListener);
    }
}
