package com.drsquad.android.provider.presenter;

import com.drsquad.android.provider.model.CustomException;
import com.drsquad.android.provider.model.ResetPasswordModel;
import com.drsquad.android.provider.model.dto.request.ResetPasswordRequest;
import com.drsquad.android.provider.model.dto.response.BaseResponse;
import com.drsquad.android.provider.model.listener.IModelListener;
import com.drsquad.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.drsquad.android.provider.view.iview.IForgotChangePasswordView;

import org.jetbrains.annotations.NotNull;


public class ForgotChangePasswordPresenter extends BasePresenter<IForgotChangePasswordView> implements IForgotChangePasswordPresenter {

    public ForgotChangePasswordPresenter(IForgotChangePasswordView iView) {
        super(iView);
    }

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }


    @Override
    public void resetPassword(ResetPasswordRequest resetPasswordRequest) {
        iView.showProgressbar();
        new ResetPasswordModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.goToLogin();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).resetPassword(resetPasswordRequest);
    }
}
