package com.drsquad.android.provider.presenter.ipresenter;

import com.drsquad.android.provider.model.dto.request.ResetPasswordRequest;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}